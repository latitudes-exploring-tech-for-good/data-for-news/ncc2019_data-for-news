# Data for News

+ *Thème :* accès à l'information.
+ *Phase d'avancement actuelle :* application disponible à l'adresse suivante https://revue-de-presse.weaving-the-web.org.
+ *Compétences associés :* developpement web.

##### #0 | Résultats obtenus lors de la Nuit du Code Citoyen
+ L'automatisation de la restitution du top 3 des publications quotidiennes de la presse française
   - Design d'un template
   - Génération d'une image à partir du template
+ Conception d'un Design System
+ D'autres ouvertures du projet
   - une navigation à partir d'un nuage de mots-clefs 
   - un récapitulatif vidéo par thématique "

##### #1 | Présentation de Data for News
Data For News s’est donné pour mission de rassembler des citoyens, développeurs, designers, data-scientists et journalistes pour penser les outils qui nous aideront à mieux comprendre l’information.

En savoir plus : [datafor.news](https://datafor.news/About.html).

##### #2 | Problématique
+ À l'heure actuelle le projet permet d'identifier les actualités françaises les plus populaires au sein du réseau social Twitter. Ces actualités sont toutes sur un plan d'égalité, quelque soit leur format : vidéo, article, brève, annonce. Dans les faits cela privilégie des médias très grand public qui misent beaucoup sur des contenus à impact fort comme la vidéo avec montage très travaillé, musique dramatique, etc.
+ Il serait beaucoup plus intéressant pour les citoyens d'accéder aux actualités populaires tout en filtrant par format pour éliminer ce biais de mise en forme "spectacle" de l'information.
+ Les publications des journaux français pourraient ainsi être qualifiées utilement. Une fois cette qualification obtenue, des filres pourraient être proposés.

##### #3 | Le défi proposé
+ Mise en place d'un modèle de qualification des publications.
+ Ajout de filtres représentant les formats des publications.
+ Visualisation des publications après application des filtres.

Langages de programmation associés aux référentiels git suivants :

+ https://github.com/thierrymarianne/daily-press-review (MySQL, PHP, RabbitMQ, Shell) - Collecte avec système de messagerie
+ https://github.com/thierrymarianne/daily-press-revue (JavaScript, CSS - Vue.js) - Restitution web
+ https://github.com/thierrymarianne/daily-press-review-golang (Golang) - Transferts des synthèse vers Firebase
+ https://github.com/thierrymarianne/daily-press-review-clojure (Clojure) - Transformation de données brutes

##### #4 | Livrables
+ Application proposant des visualisations novatrices du jeu de données initial proposé  
+ Déduction de tendances à partir du jeu de données  
+ Analyse de sentiment

##### #5 | Ressources à disposition pour résoudre le défi
https://revue-de-presse.weaving-the-web.org/nuit-du-code.csv

##### #6 | Code de conduite et philosophie du hackathon
Lors de la conception du hackathon, Latitudes a voulu prendre des partis-pris afin de rendre celui-ci particulier. 

Il s’agit d’éléments que nous souhaitons incarner collectivement avec vous tout au long des 24h :
+ La force du collectif pour faire émerger des solutions adaptées aux porteur.se.s de projets, notamment via la session de peer-learning ;
+ Une attention portée à la conception rapide, au test et à l’itération qui doivent permettre aux porteur.se.s de projets d’avoir accès aux solutions les plus abouties possibles ;
+ Le transfert de méthodes et postures, qui pourront être appliquées et transmises aux équipes par les animateur.trice.s, les mentors ET les autres équipes ;
+ L’engagement sur des solutions ouvertes, facilement utilisables par les porteur.se.s de projets, et qui vous permettront de continuer à contribuer à l’issue du hackathon si vous le souhaitez, ou de permettre à d’autres personnes de le faire.

##### #7 | Points de contact lors du hackathon
+ Sylvaine et Thierry, membres de Data for News et porteur.se.s de projet.
+ Abdelmounaim, bénévole Latitudes et en charge de la préparation du défi Data for News.
+ Yannick, co-fondateur de Latitudes.